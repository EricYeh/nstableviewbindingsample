//
//  main.m
//  NSTableViewBindingSample
//
//  Created by Vincent Wayne on 6/15/12.
//  Copyright (c) 2012 madebycocoa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
