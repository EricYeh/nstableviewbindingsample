//
//  AppDelegate.m
//  NSTableViewBindingSample
//
//  Created by Vincent Wayne on 6/15/12.
//  Copyright (c) 2012 madebycocoa. All rights reserved.
//

#import "AppDelegate.h"
#import "CameraPreset.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize presets;
@synthesize arrayController;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *prefs = [NSDictionary dictionaryWithObjectsAndKeys:[NSMutableArray array], @"MyPresets", nil];
    [defaults registerDefaults:prefs];
    
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"MyPresets"];
    if (dataRepresentingSavedArray) {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray)
            self.presets = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        else
            self.presets = [[NSMutableArray alloc] init];
    }

    // start listening for selection changes in our NSTableView's array controller
	[arrayController addObserver:self
                      forKeyPath:@"selectionIndexes" 
                         options:NSKeyValueObservingOptionNew
                         context:NULL];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:self.presets] forKey:@"MyPresets"];
    [defaults synchronize];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	NSLog(@"Table section changed: keyPath = %@, %@", keyPath, [object selectionIndexes]);
}

- (IBAction)resetPrefs:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [defaults synchronize];
}

@end
