//
//  AppDelegate.h
//  NSTableViewBindingSample
//
//  Created by Vincent Wayne on 6/15/12.
//  Copyright (c) 2012 madebycocoa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSArrayController *arrayController;
@property (retain) NSMutableArray *presets;

- (IBAction)resetPrefs:(id)sender;

@end
